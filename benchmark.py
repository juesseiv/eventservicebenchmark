#!/usr/bin/env python3

from dateutil import parser
import datetime
import json
import os
import sys
import re
import pytz
from numpy import mean


def get_immediate_subdirectories(a_dir, target='', exclude=[]):
    out = []
    for name in os.listdir(a_dir):
        if os.path.isdir(os.path.join(a_dir, name)):
            accept = True
            for veto in exclude:
                if veto in name:
                    accept = False
                    break
            if not accept:
                continue
            if target in name:
                out += [name]
    return out


def find_files_in_dir(a_dir, target='', exclude=[]):
    out = []
    for name in os.listdir(a_dir):
        if not os.path.isdir(os.path.join(a_dir, name)):
            accept = True
            for veto in exclude:
                if veto in name:
                    accept = False
                    break
            if not accept:
                continue
            if target in name:
                out += [name]
    return out

# run dir
runDir = sys.argv[1]

# main dict
job = {}

# get the PandaID
PandaID = get_immediate_subdirectories(runDir, exclude=['pilot', 'merge_es_files', 'merge_es_output', 'ray_logs'])[0]
job[PandaID] = {}
print(f"PandaID: {PandaID}")

# jobDir = runDir + PandaID
jobDir = os.path.join(runDir, PandaID)
utc = pytz.timezone("UTC")
pdt = pytz.timezone("America/Los_Angeles")
# slurm log
raythena_log = find_files_in_dir(runDir, target='raythena')[0]
slurm_log = find_files_in_dir(runDir, target='slurm')[0]
slurm_stop_time = datetime.datetime.utcfromtimestamp(
    os.path.getmtime(os.path.join(runDir, slurm_log)))
canceled_time = None
start_time = None
start_driver_time = None
stop_driver_time = None
# parse raythena log
f = open(os.path.join(runDir, raythena_log))
for l in f:
    if 'Raythena initializing' in l:
        date = l.split("|")[0]
        start_driver_time = parser.parse(date)
    elif 'All driver threads stopped. Quitting...' in l:
        stop_driver_time = parser.parse(l.split('|')[0])
f.close()
if stop_driver_time:
    stop_driver_time = utc.localize(stop_driver_time)
# parse slurm log
f = open(os.path.join(runDir, slurm_log))
for l in f:
    if not "INFO" in l and not start_time:
        continue
    if not start_time:
        date = l.split("INFO")[0]
        start_time = parser.parse(date)
    elif 'CANCELLED AT' in l and "DUE TO TIME LIMIT" in l:
        date = l.split("CANCELLED AT")[-1].split("DUE TO TIME LIMIT")[0]
        canceled_time = parser.parse(date)
f.close()

print(f"slurm log: {slurm_log}")
print(f"slurm start time: {start_time}")
print(f"slurm stop time: {slurm_stop_time}")
print(f"slurm cancel time: {canceled_time}")
print(f"driver start time: {start_driver_time}")
print(f"driver stop time: {stop_driver_time}")

AthenaMPs = get_immediate_subdirectories(jobDir, 'Actor', exclude=['esOutput'])
if not AthenaMPs:
    AthenaMPs = get_immediate_subdirectories(jobDir, 'Athena')

actors_efficiencies = []
for i, athena in enumerate(AthenaMPs):

    print(f"Processing {athena}...")

    Pilot_path = get_immediate_subdirectories(
        os.path.join(jobDir, athena), 'Pilot')
    if not Pilot_path:
        Pilot_path = get_immediate_subdirectories(
            os.path.join(jobDir, athena), 'tarball_PandaJob')
    if Pilot_path:
        base_path = os.path.join(jobDir, athena, Pilot_path[0])
    else:
        base_path = os.path.join(jobDir, athena)

    path = os.path.join(base_path, 'athenaMP-workers-AtlasG4Tf-sim')
    if os.path.isdir(path):
        dirs = [x[0] for x in os.walk(path)]
    else:
        path = os.path.join(base_path, 'athenaMP-workers-EVNTtoHITS-sim')
        dirs = [x[0] for x in os.walk(path)]

    AthenaMP = {}
    job[PandaID][athena] = AthenaMP

    # Pilot
    f = open(os.path.join(jobDir, athena, 'log.pilotwrapper'), 'r')
    pilot_start_time = None
    payload_start_time = None
    pilot_timezone = 'UTC'
    for l in f:
        if "pilot stdout BEGIN" in l:
            if not pilot_start_time:
                date = l.split("[wrapper]")[0]
                pilot_start_time = parser.parse(date)
                # pilot_timezone = pilot_start_time.strftime("%Z")
        elif "Payload started" in l:
            if not payload_start_time:
                date = l.split("|")[0]
                payload_start_time = parser.parse(date)
        if pilot_start_time and payload_start_time:
            break
    f.close()

    if not payload_start_time:
        continue

    AthenaMP['pilot'] = {}
    AthenaMP['pilot']['start time'] = pilot_start_time.strftime(
        "%m/%d/%Y, %H:%M:%S") + ", %s" % pilot_timezone
    AthenaMP['pilot']['payload time'] = payload_start_time.strftime(
        "%m/%d/%Y, %H:%M:%S") + ", %s" % pilot_timezone

    print(f"  Pilot start time {pilot_start_time}")
    print(f"  Payload start time {payload_start_time}")

    # AthenaMP
    if os.path.isfile(os.path.join(base_path, 'log.AtlasG4Tf')):
        f = open(os.path.join(base_path, 'log.AtlasG4Tf'), 'r')
    elif os.path.isfile(os.path.join(base_path, 'log.EVNTtoHITS')):
        f = open(os.path.join(base_path, 'log.EVNTtoHITS'), 'r')
    else:
        print(f"Actor {athena} has no Athena log file")
        continue
    firstLine = True
    day = ""
    athena_timezone = None
    athena_time_init = None
    for l in f:
        if firstLine:
            if 'PST' in l or 'UTC' in l or 'PDT' in l or 'CDT' in l:
                if 'CDT' in l:
                    l = l.replace('CDT', '')
                    athena_timezone = 'UTC+5'
                athena_time = parser.parse(l)
                athena_time_init = athena_time
                if not athena_timezone:
                    athena_timezone = athena_time.strftime("%Z")
                AthenaMP['init'] = {
                    'start time': athena_time.strftime("%m/%d/%Y, %H:%M:%S") + (", %s" % athena_timezone)}
                athena_day = athena_time.strftime("%m/%d/%Y")
                firstLine = False
        elif 'Waiting for sub-processes' in l:
            current_time = parser.parse(l.split(" ")[0])
            if current_time.hour < athena_time_init.hour:
                athena_day = parser.parse(athena_day).replace(day=parser.parse(athena_day).day+1).strftime("%m/%d/%Y")
            AthenaMP['init']['stop time'] = (
                "%s ," % athena_day) + current_time.strftime("%H:%M:%S") + (", %s" % athena_timezone)
            break
    f.close()
    if canceled_time:
        canceled_time = canceled_time.astimezone(athena_time.tzinfo)
    print(f"  Athena init time {AthenaMP['init']}")
    # workers
    efficiencies = []
    for d in dirs:
        if not 'worker_' in d:
            continue
        name = re.findall('(worker_[0-9]+)', d)[0]
        worker = {}
        f = open(os.path.join(d, 'AthenaMP.log'), 'r')
        firstLine = True
        total_latency = 0
        total_processing = 0
        firstEvent = True
        init_time = None
        current_time = None
        last_time = None
        for l in f:
            if 'start processing event' in l:
                event = re.findall('start processing event #([0-9]+)', l)[0]
                last_time = current_time
                current_time: datetime.datetime = parser.parse(l.split("AthenaEventLoopMgr")[0])
                worker['event%s' % event] = {'start time': current_time.strftime(
                    "%m/%d/%Y, %H:%M:%S") + ", %s" % athena_timezone}
                if firstEvent:
                    firstEvent = False
                    worker['init']['stop time'] = current_time.strftime(
                        "%m/%d/%Y, %H:%M:%S") + ", %s" % athena_timezone
                else:
                    total_latency += (current_time - last_time).total_seconds()

            elif 'done processing event' in l:
                event = re.findall('done processing event #([0-9]+)', l)[0]
                last_time = current_time
                current_time = parser.parse(l.split("AthenaEventLoopMgr")[0])
                total_processing += (current_time - last_time).total_seconds()
                worker['event%s' % event]['stop time'] = current_time.strftime(
                    "%m/%d/%Y, %H:%M:%S") + ", %s" % athena_timezone
            elif firstLine:
                firstLine = False
                init_time = parser.parse(l.split("AthMpEvtLoopMgr")[0])
                worker['init'] = {'start time': init_time.strftime(
                    "%m/%d/%Y, %H:%M:%S") + ", %s" % athena_timezone}
        if not firstEvent and current_time:
            current_time = current_time.astimezone(athena_time.tzinfo)
            if stop_driver_time:
                idle_time = (stop_driver_time - current_time).total_seconds()
                total_latency += idle_time
            elif canceled_time:
                idle_time = (canceled_time - current_time).total_seconds()
                total_latency += idle_time
        if total_processing > 0:
            efficiency = 1 - total_latency / (total_latency + total_processing)
            worker['efficiency'] = efficiency
            efficiencies.append(efficiency)
        else:
            worker['efficiency'] = -1
        f.close()
        AthenaMP[name] = worker
    
    mean_efficiency = mean(efficiencies)
    AthenaMP['efficiency'] = mean_efficiency
    actors_efficiencies.append(mean_efficiency)
    print(f"Mean efficiency for {athena}: {mean_efficiency}")

job[PandaID]['efficiency'] = mean(actors_efficiencies)
print(f"Overall job efficiency: {job[PandaID]['efficiency']}")
# Which timezone for slurm logs?
job[PandaID]['start time'] = start_time.strftime(
    "%m/%d/%Y, %H:%M:%S") + ", %s" % (pilot_timezone if abs(start_time.hour - pilot_start_time.hour) < abs(start_time.hour - athena_time.hour) else athena_timezone)
job[PandaID]['driver start time'] = start_driver_time.strftime(
    "%m/%d/%Y, %H:%M:%S") + ", %s" % (pilot_timezone if abs(start_driver_time.hour - pilot_start_time.hour) < abs(start_driver_time.hour - athena_time.hour) else athena_timezone)
if canceled_time:
    job[PandaID]['canceled time'] = canceled_time.strftime(
        "%m/%d/%Y, %H:%M:%S") + ", %s" % athena_timezone

# Always UTC read from timestamp
job[PandaID]['stop time'] = slurm_stop_time.strftime(
    "%m/%d/%Y, %H:%M:%S") + ", %s" % "UTC"

json_data = json.dumps(job)
f = open(f"{PandaID}.json", 'w')
f.write(json_data)
f.close()
